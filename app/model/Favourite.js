Ext.define('urshuttle.model.Favourite', {
    extend: 'Ext.data.Model',
    config: {
        fields: ['fromId', 'fromName', 'toId', 'toName'],
    }
});