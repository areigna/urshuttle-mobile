Ext.define('urshuttle.model.Station', {
    extend: 'Ext.data.Model',
	config: {
		fields: [
			{name: "id",  type: "int"},
			{name: "stationName", type: "string"},
			{name: "click",  type: "int"},
		]
    }
});

