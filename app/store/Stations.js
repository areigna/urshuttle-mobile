Ext.define('urshuttle.store.Stations', {
    extend: 'Ext.data.Store',
	requires: ['urshuttle.model.Station'],
	config: {
		model: 'urshuttle.model.Station',
		storeId: "stationsStore",
		autoLoad: true,
		sorters: [
			{
				property : "click",
				direction: "DESC",
			},
			{
				property : "stationName",
				direction: "ASC",
			}
		],
		grouper: {
	       groupFn: function(record) {
	           return record.get('stationName')[0];
	       }
   	    },

   	    proxy: {
   	    	type : 'ajax',
   	    	url : 'data/stations.json',
   	    	reader: {
                type: 'json',
                rootProperty: 'value',
            }
   	    }
   	   /*
		data : [
			{stationName: "Rush Rhees Library",    click: 100},
			{stationName: "Southside/University Park",    click: 10},
			{stationName: "Eastman Living Center/Eastman",    click: 10},
			{stationName: "Riverview Apartments",    click: 10},
			{stationName: "Rush Rhees Library",    click: 0},
			{stationName: "Southside/University Park",    click: 0},
			{stationName: "Eastman Living Center/Eastman",    click: 0},
			{stationName: "Riverview Apartments",    click: 0}
		]
		*/
    }
});
