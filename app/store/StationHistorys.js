Ext.define('urshuttle.store.StationHistorys', {
    extend: 'Ext.data.Store',
	requires: ['urshuttle.model.Station','Ext.data.proxy.LocalStorage'],
	config: {
		model: 'urshuttle.model.Station',
		storeId: "stationHistorysStore",
		autoLoad: true,
		autoSync: true,
   	    proxy: {
   	    	type : 'localstorage',
   	    	id: 'station',
   	    }
   	   /*
		data : [
			{stationName: "Rush Rhees Library",    click: 100},
			{stationName: "Southside/University Park",    click: 10},
			{stationName: "Eastman Living Center/Eastman",    click: 10},
			{stationName: "Riverview Apartments",    click: 10},
			{stationName: "Rush Rhees Library",    click: 0},
			{stationName: "Southside/University Park",    click: 0},
			{stationName: "Eastman Living Center/Eastman",    click: 0},
			{stationName: "Riverview Apartments",    click: 0}
		]
		*/
    }
});
