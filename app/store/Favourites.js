Ext.define('urshuttle.store.Favourites', {
    extend: 'Ext.data.Store',
	requires: ['urshuttle.model.Favourite','Ext.data.proxy.LocalStorage'],
	config: {
		model: 'urshuttle.model.Favourite',
		storeId: "favouritesStore",
		autoLoad: true,
		autoSync: true,
		sorters: [
			{
				property : "stationName",
				direction: "ASC"
			}
		],
        proxy: {
            type: 'localstorage',
            id  : 'favourite',
		}
    }
});
