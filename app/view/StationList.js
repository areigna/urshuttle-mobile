Ext.define('urshuttle.view.StationList', {
    extend: 'Ext.dataview.List',
    xtype: 'stationlist',
    requires: [
    	'Ext.device.Device',
    ],
    config: {
		store: "stationsStore",
		itemTpl: "{stationName}",
		grouped     : true,
	    indexBar    : true,
		listeners:{
			select: function(view, record) {
				if(view.getId()=="fromList"){
					Ext.getCmp('from').setText(record.get('stationName'));
					Ext.getCmp('to').reset();
					}
				else{
					Ext.getCmp('to').setText(record.get('stationName'));
					//Ext.getCmp('resultList').show({type:'slide'});
					Ext.getCmp('nav').push({
						xtype: 'resultlist',
					});
				}
			}
		},
		reset: function(){
			this.deselectAll();
		},
	},
});
