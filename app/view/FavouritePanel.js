Ext.define('urshuttle.view.FavouritePanel', {
    extend: 'Ext.dataview.List',
    xtype: 'favouritepanel',
    required: [
	    //'urshuttle.view.Main',
	    //'urshuttle.view.ResultList',
    ],
    config: {
    	id: 'favouritepanel',
    	store: 'favouritesStore',
        loadingText: 'loading..',
        mode: 'SINGLE',
        //iconCls: 'star',
		itemTpl: [
            '<div>',
            '{fromName} <br/> --> {toName}',
            //'<button class="removeFavourite" style="float:right;color:#999">X</button>',
            '</div>'
            ].join(''),
        onItemDisclosure : function(record){
            var store = Ext.getStore('favouritesStore');
            store.remove(record);
        },

    },
});
