Ext.define('urshuttle.view.SearchPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'searchpanel',
    required: [
    ],
    config: {
    	id: 'searchpanel',
    	items:[
    		{
    			xtype: 'fieldset',
    			//title: 'Select Stations',
    			instructions: '',
    			defaults: {
	    			xtype: 'selectfield',
	    			usePicker: false,

					store: "stationsStore",
					valueField: 'id',
					displayField: 'stationName',
    			},
    			items: [
		    		{name: 'fromId', label: 'From:', },
		    		{name: 'toId', label: 'To:', },
    			]
    			
    		},
    		/*
    		{
    			xtype: 'fieldset',
    			items:[
	    			{
		    			xtype: 'checkboxfield',
		    			name: 'favourite',
		    			label: 'Add to Favourite:',
	    			}
    			],
    		},
    		*/
    		{
    			xtype: 'fieldset',
    			items:[
	    			{
		    			xtype: 'button',
		    			id: 'searchButton',
		    			text: 'Search',
		    			ui: 'confirm',
		    			handler: function(){
		    				var form = this.up('formpanel');
		    				var fromData = form.down('[name=fromId]').getRecord()
		    				var toData = form.down('[name=toId]').getRecord()
		    				var data = form.getValues();
		    				//alert(JSON.stringify(this.up('formpanel').getValues(),null,2));
    						var store = Ext.getStore('favouritesStore');
    						store.add({
    							fromId: fromData.get('id'),
    							fromName: fromData.get('stationName'),
    							toId: toData.get('id'),
    							toName: toData.get('stationName'),
    						});
		    			}
	    			}
    			],
    		},
    	],
    	/*
		navigationBar: {
	        hidden: true
	    },
	    */
    },
});
