Ext.define('urshuttle.view.StationSelect', {
    extend: 'Ext.Button',
    xtype: 'stationselect',
    requires: [
    ],
    config: {
    	stationId: null,
    	originalText: null,
    	listId: null,
	},
	initialize: function(){
		this.callParent(arguments);
        this.element.on('tap', this.reset, this);
        this.setOriginalText(this.getText());
		this.setListId(this.getId()+'List');
	},
	reset: function(){
			this.setText(this.getOriginalText());
			this.setStationId(0);
			Ext.getCmp(this.getListId()).deselectAll();
			this.selectShow();
	},
    selectShow: function(){
		var cmp = Ext.getCmp(this.getId()+"List");
		if(cmp.isHidden()){
			Ext.getCmp('resultList').hide();
			Ext.getCmp('toList').hide();
			Ext.getCmp('fromList').hide();
			cmp.show({type:'fade'} );
		}
	}
});
