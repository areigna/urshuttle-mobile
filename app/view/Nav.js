Ext.define('urshuttle.view.Nav', {
    extend: 'Ext.navigation.View',
    xtype: 'nav',
    required: [
	    'urshuttle.view.Main',
	    'urshuttle.view.ResultList',
    ],
    config: {
    	id: 'nav',
    	fullscreen: true,
		navigationBar: {
	        hidden: true
	    },
    	items: 
    	[
    		{
		    	xtype: 'main',
    		},
    	],
    },
});
