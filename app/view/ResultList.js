Ext.define('urshuttle.view.ResultList', {
    extend: 'Ext.field.Select',
    xtype: 'resultlist',
    requires: [
    	'Ext.Toolbar'
    ],
    config: {
		store: "stationsStore",
		label: 'Choose one',
		valueField: 'click',
		displayField: 'stationName',
		items:
		[
			{
				xtype: 'toolbar',
				docked: 'top',
				items:[
					{
						text:'back',
						ui:'back',
						listeners:{
							tap: function(){
								Ext.getCmp('nav').pop();
							},
						},
					},
					{
						xtype: 'spacer',
					},
					{
						iconCls:'star',
					},
				],
			}
		],
	},
});
