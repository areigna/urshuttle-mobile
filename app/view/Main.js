Ext.define('urshuttle.view.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'main',
    requires: [
        'urshuttle.view.SearchPanel',
        'Ext.form.Panel',
        'Ext.form.FieldSet',
        'Ext.dataview.List',
        'Ext.SegmentedButton'
    ],
    config: {
        tabBarPosition: 'bottom',
        id: 'main',
		layout: {
			type:'card',
			animation: 'fade'
		},
		defaults:{
			flex:1
		},

        items: [
			{
				xtype:'searchpanel',
				iconCls:'search',
				title:'Search',
			},
            {
            	xtype: 'favouritepanel',
                iconCls: 'star',
                title: 'Favourite',

            },
            {
                title: 'Report',
                iconCls: 'compose',
				xtype:'formpanel',
				items: [
					{
						xtype: 'fieldset',
						items: [
							{
								xtype: 'textfield',
								label: 'Email',
								placeHolder: 'Email address..',
								name: 'email'
							},
							{
								xtype: 'textareafield',
								label: 'Info',
								placeHolder: 'Tell us about the wrong bus info..',
								maxRows: 4,
								name: 'info'
							}
						]
					}
				]

            }
        ],
    }
});
/*
        function selectShow(cmp){
			Ext.msg.alert('i was painted to the screen'); 
			if(cmp.ishidden()){
				Ext.getcmp('resultlist').hide();
				Ext.getcmp('tolist').hide();
				Ext.getcmp('fromlist').hide();
				cmp.show({type:'slide',direction:'right'} );
			}
		}
		*/
